<?php
use App\Helpers\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('orders', 'OrdersController')->middleware('cors');

Route::get('/customers_list', function () {
    return Customer::get_customers();
});

Route::get('/products_by_customer/{id}', function ($id) {
    return Customer::getProdctByCustomer($id);
});

Route::get('/products_detail/{id}', function ($id) {
    return Customer::getProdctDetail($id);
});