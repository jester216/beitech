<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class OrdersController extends Controller
{

    /**
     * Display a listing of Orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customer = $request->input('customer');
        $start = $request->input('start','1990-01-01');
        $end = $request->input('end','2040-01-01');
        
        $data = Order::select("order.order_id", "customer.name", "order.creation_date", "order.delivery_address", "order.total");
        $data->selectRaw('GROUP_CONCAT(product.name) as products ')
            ->join('customer', 'order.customer_id', '=', 'customer.customer_id')
            ->join('order_detail', 'order.order_id', '=', 'order_detail.order_id')
            ->join('product', 'product.product_id', '=', 'order_detail.product_id')
            ->groupBy('order.order_id')
            ->groupBy('order.creation_date')
            ->groupBy('order.delivery_address')
            ->groupBy('order.total')
            ->groupBy('customer.name')
            ->orderBy('order.creation_date', 'desc')
            ->limit(15);
            
        if(($start != '' && $start != null) && ($end != '' && $end != null)){
            $data->whereBetween('order.creation_date', [$start . " 00:00:00", $end . " 23:59:59"]);
        }
        if($customer != null && $customer > 0){
            $data->where('order.customer_id', $customer);
        }
        
        $data = $data->get();
        //$data = Order::limit(25)->get();
        $response = array('status' => true, 'data' => array());
        if (count($data) > 0) {
            $response = array('status' => true, 'data' => $data);
        }
        echo json_encode($response);
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created Order in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $return = array('status' => false);
        $order = $request->get('jsonOrder');
        $prods = $request->get('products');

        $validateOrders = \Validator::make($order, [
            'delivery_address'      =>  'required',
            'customer_id'      =>  'required|numeric',
        ]);
        if (!empty($order) && $order != null) {

            if ($validateOrders->fails()) {
                $return['message'] = 'Ha ocurrido un error de validación de datos';
                $return['code'] = 404;
                $return['error'] = $validateOrders->errors();
            } else {
                if (count($prods) > 0) {

                    $orderObj = new Order();
                    $orderObj->customer_id = $order['customer_id'];
                    $orderObj->creation_date = Carbon::now('America/Bogota');
                    $orderObj->delivery_address = $order['delivery_address'];
                    $orderObj->total = $order['total'];

                    $orderObj->save();

                    foreach ($prods as $key => $product) {
                        $detail = new OrderDetail();
                        $detail->order_id = $orderObj->id;
                        $detail->product_id = $product['product_id'];
                        $detail->product_description = $product['product_description'];
                        $detail->price = $product['price'];
                        $detail->quantity = $product['quantity'];
                        $detail->save();
                    }

                    $return['status'] = true;
                    $return['message'] = 'Orden creada correctamente';
                    $return['code'] = 200;
                }
            }
        }

        
        return response()->json($return, $return['code']);

    }


    /**
     * Show the form for editing Order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update Order in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove Order from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
