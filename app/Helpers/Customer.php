<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
 
class Customer {
    /**
     * 
     * @return string
     */
    public static function get_customers() {
        $customers = DB::table('customer')->pluck('name','customer_id');
        
        $status = false;
        $data = [];
        if(count($customers)>0){
            $status =true;
            foreach($customers as $key => $row){
                $data[] = array('id' => $key, 'option' => $row);
            }
        }
        $return = array('status' => $status, 'data'=> $data);
             
        return json_encode($return);
    }
    public static function getProdctByCustomer($id) {
        $customers = DB::table('product')
        ->select('product.*')
        ->join('customer_product','customer_product.product_id','=','product.product_id')
        ->where('customer_product.customer_id',$id)
        ->pluck('name','product_id');
        
        $status = false;
        $data = [];
        if(count($customers)>0){
            $status =true;
            foreach($customers as $key => $row){
                $data[] = array('id' => $key, 'option' => $row);
            }
        }
        $return = array('status' => $status, 'data'=> $data);
             
        return json_encode($return);
    }
    public static function getProdctDetail($id) {
        $products = DB::table('product')->where('product_id',$id)->get();
        
        $status = false;
        $data = [];
        if(count($products)>0){
            $status =true;
            foreach($products as $key => $row){
                $data = array('id' => $row->product_id, 'name' => $row->name,'product_description'=>$row->product_description,'price' => $row->price);
            }
        }
        $return = array('status' => $status, 'data'=> $data);
             
        return json_encode($return);
    }
}